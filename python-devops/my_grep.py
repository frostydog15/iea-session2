#!/usr/bin/env python3

import re
import sys
from os import path
def assign_vars():
    for idx, arg in enumerate(sys.argv):
        print(idx, " ", arg)
    if len(sys.argv) != 3:
        print('Parameters: FileName, regex to search')
        sys.exit(-1)

    try:
        path.exists(sys.argv[1])
    except FileNotFoundError:
        print("File {} not found".format(sys.argv[1]))
        sys.exit(-2)
    else:
        file_name = sys.argv[1]
    try:
        regex_pattern = str(sys.argv[2])
    except TypeError:
        print("Wrong type for regex pattern. Could not convert to string")

    advanced_regex_search(file_name, regex_pattern)
    

def pattern_search(file_name, regex_pattern):
    try:
        file = open(file_name, "r")
    except FileNotFoundError:
        print("File Not Found")
        sys.exit(-3)
    match_line = [line for line in file
                    if regex_pattern in line ]
    for line in match_line:
        for word in line.split():
            if regex_pattern in word:
                print("\033[1;31m", end = '')
                print(word, end = ' ')
                print("\033[00m,", end = '')
            else:
                print(word, end = ' ')
        print()
    file.close()
    
def advanced_regex_search(file_name, regex_pattern):
    
    try:
        file = open(file_name, "r")
    except FileNotFoundError:
        print("File Not Found")
        sys.exit(-4)

    linenum = 0
    for line in file:
        linenum += 1
        if re.search(regex_pattern, line):
            print(linenum, ': ', line)
            
    file.close()


if __name__ == '__main__':
    print("Running as script")
    assign_vars()
else:
    print("Importing")


