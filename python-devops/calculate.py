#!/usr/bin/env python3

import math
import sys

def calculate(operand1, operand2, operator):
    if operator == '+':
        return operand1 + operand2
    elif operator == '-':
        return operand1 - operand2
    elif operator == '*':
        return operand1 * operand2
    elif operator == '/':
        try:
            return operand1 / operand2
        except ZeroDivisionError:
            print("You cannot divide by zero")
    elif operator == 'log':
        try:
            log_a = math.log(operand1)
            log_b = math.log(operand2)
        except ZeroDivisionError:
            print("Zero Division Error: Seccond operand cannot be 1. Log(1) = 0")
        except ValueError:
            print("Domain Error. Log of any number less than or equal to 0 is undefined")
        else:
            return calculate(log_a, log_b, '/')
        finally:
            print("Log Operation Complete")


for idx, arg in enumerate(sys.argv):
    print(idx, " ", arg)
value = calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3])
print(value)
