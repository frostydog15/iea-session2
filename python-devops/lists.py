#!/usr/bin/env python3

lists1 = []
lists2 = []
print("Enter 1 to quit entering input")
while True:
    option = input("Enter an option a to add, r to remove, c to compare, 1 to quit")
    print(lists1)
    print(lists2)
    if option == 'a':
        list_num = int(input("which list to add?"))
        if list_num == 1:
            user_string = str(input("Enter a string to add to list 1: "))
            lists1.append(user_string)
            print(lists1)
        elif list_num == 2:
            user_string = str(input("Enter a string to add to list 2: "))
            lists2.append(user_string)
            print(lists2)
    elif option == 'r': 
        list_num = int(input("which list to remove?"))
        if list_num == 1:
            print(lists1)
            user_string = str(input("Remove from list 1 by v, value or by i, index? "))
            if user_string == 'i':
                index_val = int(input("Enter an index value: "))
                del lists1[index_val]
                print(lists1)
            elif user_string == 'v':
                string_val = str(input("Enter a string value: "))
                lists1.remove(string_val)
                print(lists1)
        if list_num == 2:
            print(lists2)
            user_string = str(input("Remove from list 2 by v, value or by i, index? "))
            if user_string == 'i':
                index_val = int(input("Enter an index value: "))
                del lists2[index_val]
                print(lists2)
            elif user_string == 'v':
                string_val = str(input("Enter a string value: "))
                lists2.remove(string_val)
                print(lists2)
    elif option == 'c':
        list1_sorted = sorted(lists1)
        list2_sorted = sorted(lists2)
        print("List 1 and list 2 equality is ", list1_sorted == list2_sorted)
        print(list1_sorted)
        print(list2_sorted)
    elif option == '1':
        break
