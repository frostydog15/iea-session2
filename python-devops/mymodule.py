#!/usr/bin/env python3

def dummy():
    return 45

def foo():
    print('bar')
    return 1

public_data = "public stuff"

_private_data = "private stuff"

if __name__ == '__main__':
    print("Running as script")
    dummy()
    foo()
else:
    print("Imported")

