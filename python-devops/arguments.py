#!/usr/bin/env python3
import sys
print('Program arguments', sys.argv)

if __name__=="__main__":
    print("Running as script. __name__ = ", __name__)
else:
    print("Imported as: ", __name__)
